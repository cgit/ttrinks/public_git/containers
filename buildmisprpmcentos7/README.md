﻿[buildmisprpm]
==============

Initial goal:

Automate (as in containerise) the build of relevant MISP related (S)RPM on and for Centos 7. This currently works with this setup as of Fri Jul 22 12:03:51 AM UTC 2022, using the MISP (and for pre-requisite reasons FAUP and GTCACA) SPEC and SOURCES files available at that time [1][2] and results in a container image that has all relevant MISP related RPMs pre-built before starting a podman container.

Ultimate goal:

Attempt to have relevant MISP related1 RPM included in EPEL.

This currently seems to have at least two blockers (see below under "Running") that prevent a successful inclusion to Fedora EPEL and that's the lack of pre-packaged gtaca\* or faup\* packages - getting them properly added to any official repository (e.g. EPEL) is an essential pre-requisite before even attempting to build MISP related RPMs for EPEL.

Like the MISP RPMs, both gtaca\* or faup\* related packages are currently built as part of the image building (there are no RPMs anywhere in "official" RPM repos such as RPMFUSION, EPEL, etc. for those very specific packages) and are based on the MISP upstream SPEC files [1].

Another idea is whether it is feasible to actually run a MISP podman container, using this image with the already pre-built RPMs as an INSTALL base but at this stage it is unclear whether all components (database, specific php and httpd packages, etc.) can all be run in a podman container.

[1] https://github.com/MISP/MISP-RPM/tree/rhel79/SPECS
[2] https://github.com/MISP/MISP-RPM/tree/rhel79/SOURCES

Requirements
------------

This work has been developed and tested on Fedora with podman/buildah. For the image itself quay.io/centos/centos:7 is used.

Prerequisites
-------------

Loads. See Dockerfile for some details.

Instructions for use
--------------------

Building:

```
cd buildmisprpmcentos7
buildah bud -t buildmisprpmcentos7 . && podman image prune -f
```

Running:

```
podman run -it --rm buildmisprpmcentos7
```

The RPMs built as part of the container image build (see previous step "Building") are available as a tar.gz archive in /tmp - an initial test installation of those RPMs on a dedicated Centos7 test vm as per INSTALL.md [1] was successful (see screenshot file misp_centos7.png in this repository).

However, they fail to build (e.g see screenshot files gtcaca_koji_scratch_build_0{1,2,3}.png in this repository) on the official Fedora Build Environment (https://koji.fedoraproject.org/) at this stage, possibly due to not meeting the Fedora "EPEL Guidelines and Policies" [2] or other Fedora build specific requirements.

A complete walk-through (in a terminal) is available to watch as an ASCIINEMA file (asciinema play build_misp_et_al_rpms_for_centos7_in_a_container.cast) in this repository.


[1] https://github.com/MISP/MISP-RPM/blob/rhel79/INSTALL.md
[2] https://fedoraproject.org/wiki/EPEL/GuidelinesAndPolicies#Policy

License
-------

GPLv3

Author Information
------------------

Timo Trinks, ttrinks@fedoraproject.org

Acknowledgements
------------------

N/A
