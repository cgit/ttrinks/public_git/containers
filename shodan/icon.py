#!/usr/bin/env python3
# -*- coding: utf-8 -*-
""" mmh3 favicon hash """

import codecs
import requests
import mmh3

print('Please enter the favicon site to hash (e.g. www.google.com): ')
fav = input()
URL = "https://"+f"{fav}"+"/favicon.ico"
print('The hash for ' + str(URL) + ' is: ')
response = requests.get(URL, data=None, headers={'User-Agent': 'Mozilla/5.0 (X11; Linux; rv:113.0) Gecko/20100101 Firefox/113.0'}, timeout=10)
favicon = codecs.encode(response.content,"base64")
favhash = mmh3.hash(favicon)
print(favhash)
