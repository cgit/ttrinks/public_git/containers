#!/bin/sh

set -eu

# init shodan then exit into bash
export PATH=$PATH:/root/.local/bin/
shodan init ${shodanapikey}
export shodanapikey=nuked
/bin/bash
