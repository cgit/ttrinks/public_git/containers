[shodan]
========

Create a Shodan container that's ready to roll.

Requirements
------------

This work has been developed and tested on Fedora with podman/buildah Fedora (latest) was chosen as it meets all required dependencies.

Prerequisites
-------------

The following steps are required ONCE (on the machine the container runs) prior to building and running the container:

- export SHODANAPIKEY=(insert api key id used for upload here)
- podman secret create --env shodanapikey SHODANAPIKEY

Instructions for use
--------------------

Building:

```
cd shodan
buildah bud -t shodan . && podman image prune -f
```
Running:

```
podman run -it --secret source=shodanapikey,type=env --rm shodan
```

Another tool that comes with it is icon.py to murmur hash a website's favourite icon.

License
-------

GPLv3

Author Information
------------------

Timo Trinks, ttrinks@fedoraproject.org

Acknowledgements
------------------

N/A
