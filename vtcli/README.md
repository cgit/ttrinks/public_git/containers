[vtcli]
=======

Containerised (Podman) Virus Total client that uses an API stored as Podman Secret (sekurrre!) on the executing machine (so one a) won't have to store the API key in plain nor b) provide it during runtime).

Requirements
------------

This work has been developed and tested on Fedora with podman/buildah.

Prerequisites
-------------

The Virus Total client uses an API key that can be stored as environment variable during container runtime.

In order to avoid hardcoding credentials "podman secret" is being used.

The following steps are required ONCE (on the machine the container runs) prior to building and running the container:

- export VTAPIKEY=(insert api key used for access here)
- podman secret create --env VirustotalToken VTAPIKEY

Instructions for use
--------------------

Building:

```
cd vtcli
buildah bud -t vtcli . && podman image prune -f
```

Running:

```
podman run -it --secret source=VirustotalToken,type=env,target=VTCLI_APIKEY --rm vtcli [command] (see vtcli --help for options)
```

License
-------

GPLv3

Author Information
------------------

Timo Trinks, ttrinks@fedoraproject.org

Acknowledgements
------------------

N/A
